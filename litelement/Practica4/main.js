class MiMensaje extends HTMLElement {
	constructor() {
		super();
		this.addEventListener('click', function (e) {
			alert('Click en mensaje');
		});
		console.log('Constructor: cuando el elemento es creado');
	}
	//propiedad para escuchar solo los atributos definidos en el arreglo usando
	//attributeChanvedCallback
	static get observedAttributes() {
		return ['msj'];
	}
	//callback cuando se inserta el elemento e
	connectedCallback() {
		console.log('connectedCallback: cuando el elemento es adoptado por otro documento');
	}
	//Cuando un atributo es modificado, solo llamado en atributos observados definidos en
	//la propiedad observedAttributes
	attributeChangedCallback(attrName, oldVal, newVal) {
		console.log('attributeChangedCallback: Cuando cambia un atributo');
		if (attrName === 'msj') {
			this.pintarMensaje(newVal);
		}
	}

	//Pintar el mensaje que se declara en el atributo 'msj'
	pintarMensaje(msj) {
		this.innerHTML = msj;
	}
}
customElements.define('mi-mensaje', MiMensaje);

let segundoMensaje = document.createElement('mi-mensaje');
segundoMensaje.msj = 'Otro mensaje';
document.body.appendChild(segundoMensaje);

//Tambien puedes crear un elemento con el operador new
let tercerMensaje = new MiMensaje();
tercerMensaje.msj = 'Tercer mensaje';
document.body.appendChild(tercerMensaje);