class miMensaje extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            alert('click en mensaje');
        });
        console.log('constructor: cuando el elemento es creado');
    }

    //propiedad para escuchar solo los atributos definidos en el arreglo usando
    // attributeChangedCallback
    static get observedAttributes(){
        return['msj'];
    }
    //callback cuando se inserta el elemento en el dom
    connectedCallback(){
        console.log('connectedCallback: cuando el elemento es insertado en el documento');
    }
    disconnectedCallback(){
        alert('disconnected: cuando el elemento es eliminado del documento')
    }
    adoptedCallback(){
        alert('adoptedclaback: cuando el elemento es adoptado por otro documento');
    }
    //cuando un atributo es modificado, solo llamado en atributos observados definidos en
    //la propiedad observedAttributes
    attributeChangedCallback (attrName, oldVal, newVal){
        console.log('attributechangedcalback: cuando cambia un atributo');
        if (attrName==='msj') {
            this.pintarMensaje(newVal);
        }
    }

    //pintar el mensaje que se declara en el atributo 'msj'
    pintarMensaje(msj){
        this.innerHTML = msj
    }
    get msj(){
        return this.getAttribute('msj');
    }
    set msj(val){
        this.setAttribute('msj',val);
    }
}
customElements.define('mi-mensaje', miMensaje);

let miMensaje1 = document.createElement('mi-mensaje')
miMensaje1.msj = 'otro mensaje';
document.body.appendChild(miMensaje1);

//tambien puedes crear un elemento con el operador new
let tercerMensaje = new miMensaje();
tercerMensaje.msj = 'tercer mensaje';
document.body.appendChild(tercerMensaje);