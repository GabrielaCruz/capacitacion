MVC.View = class View{
    constructor(elem){
        //que escuche primero los elementos
        this.eventHandler();

        this.elem = elem;
    }
    eventHandler(){
        document.body.addEventListener('onLoadData', (event) =>{
            this.updateView(event.datail);
        });
    }
    notify (data){
        const onLoadDataEvent = new CustomEvent("onLoadData", {datail: data, bubbles: true});
        this.elem.dispatchEvent(onLoadDataEvent);
    }
    updateView(){
        for (let key in datos) {
            const node = this.elem.querySelector(`#${key}`);
            if (node != null) {
                console.log(node);
                node.value=datos[key];
            } else {
                console.log(`no se encontro ${key}`);
            }
        }
    }
    
}