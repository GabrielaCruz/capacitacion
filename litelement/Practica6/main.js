class MiSaludo extends HTMLElement {
    constructor(){
        //OBTENGO LA UNICA ETIQUETA 'TEMPLATE'
        const tpl = document.querySelector('template');
        //clono su contenido y se crea una instancia de document fragment
        const tpInst = tpl.content.cloneNode(true);

        super();//invoca el constructor de la clase padre
        //se crea un shado dom para las instancias de mi-saludo
        this.attachShadow({mode: 'open'});
        //y se agrega el template dentro del shadow dom usando el elemento raiz 'shadowroot'
        this.shadowRoot.appendChild(tpInst);
    }
}
//se registra el custom element para poder ser utilizado declarativamente en el html
//o imperativamente mediante js
customElements.define('mi-saludo',MiSaludo);