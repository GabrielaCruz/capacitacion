//ejercicio 1.1
function f(x, y=2, z=7){
    return x+y+z;
}
console.log('ejercicio 1.1: '+f(5,undefined)); //resultado   14

//ejercicio 1.2
var animal='kitty';
var result= (animal ==='kitty') ? 'cute' : 'still nice';
console.log('ejercicio 1.2: '+result);//resultado   cute

//ejercicio 1.3
function x(){
    var animal1= 'kitty';
    var result1= '';
    if (animal1==='kitty') {
        result1='cute';

    } else {
        result1='still nice';
    }
    console.log('ejercicio 1.3: '+result1);//el resultado es cute
}
x();

//ejercicio 2.1
function y(){
    var a=0;
    var str= 'not a';
    var b = '';
    b = a === 0 ? (a = 1, str += ' test') : (a = 2);
    console.log('ejercicio 2.1: '+a+'  '+str); // resultado    not a test
}
y();

//ejercicio 2.2
function c(){
    var a=1;
    a===1 ? alert('hey, it is 1!'): 0;
    console.log(a); //resultado manda una alarma con el texto
}
//c();

//ejercicio 2.3
function d(){
    var a;
    //a === 1  alert('hey, it is 1!') : alert('weird, what could it be?');
 //   if(a===1) alert('hey, it is 1!'); else alert('weird, what could it be?');
    // resultado  weird, what could it be?
}
d();

//ejercicio 3.1
function a3(){
    var animal = 'kitty';
    for (var i = 0; i < 5; i++) {
        if (animal ==='kitty'){
            break;
        }else{
            console.log(i);
        }
        //resultado  no imprime nada ya que cierra en el if, ya que si es kitty
    }
}
a3();

//ejercicio 3.2
function a2(){
    var value=1;
    switch (value) {
        case 1:
            console.log('i will always run')
            break;
    
        case 2:
            console.log('i will never run')
            break;
    }
    // resultado i will always run
}
a2();
//jercicio 4.1
function ani(){
    var animal='Lion';
    switch (animal) {
        case 'dog':
           console.log('!==dog') 
            break;
        case 'Cat':
            console.log('!==cat') 
            break;
        default:
            console.log('otro animal') 
    }
    //resultado otro animal
}
ani();

//ejercicio 4.2
function name() {
    function john() {
        return 'john';
    }
    function jacob(){
        return 'jacob';
    }
}

//ejercicio 5.1
function e2(){
    var x="c";
    switch (x) {
        case "a":
        case "b":
        case "c":
            console.log("either a, b, or c was selected")
            break;
        case "d":
            console.log("Only d was select");
            break;
        default:
            console.log("no case was matched");
            break;
    }
    // resultado either a, b, or c was selected
}
e2();

//ejercicio 5.2
function cuenta(){
    var x = 5 + 7;
    var x1 = 5 + "7";
    var x2 = "5" + 7;
    var x3 = 5 - 7;
    var x4 = 5 - "7";
    var x5 = "5" - 7;
    var x6 = 5 - "x";

    console.log(x+'  '+x1+'  '+x2+'  '+x3+'  '+x4+'  '+x5+'  '+x6);
}
cuenta();

//ejercicio 6.1
function letra(){
    var x = 'hello' || '';
    var x1 = '' || [];
    var x2 = 'hello' || undefined;
    var x3 = 5 - 7;
    var x4 = 1 || 5;
    var x5 = 0 || {};
    var x6 = 0 || '' || 5;
    var x7 = ''|| 'yay' || 'boo';

    console.log(x+'  '+x1+'  '+x2+'  '+x3+'  '+x4+'  '+x5+'  '+x6+'  '+x7);
}
letra();

//ejercicio 6.2
function sig(){
    var x = 'hello' && '';
    var x1 = '' && [];
    var x2 =  undefined && 0;
    var x3 = 1 && 5;
    var x5 = 0 && {};
    var x6 = 'hi' && [] && 'dome';
    var x7 = 'bye'&& undefined || 'adios';

    console.log(x+'  '+x1+'  '+x2+'  '+x3+'  '+x5+'  '+x6+'  '+x7);
}
sig();


//ejercicio 7.1
function fo(){
    var foo = function(val){
        return val || 'default';
    }
    console.log('----------------------');
    console.log(foo('burger'));
    console.log(foo(100));
    console.log(foo([]));
    console.log(foo(0));
    console.log(foo(undefined));
    console.log('----------------------');
}
fo();


//ejercicio 7.2
/*function is(){
    var isLegal = age >= 18;
    var tall = height >= 5.11;
    var suitable = isLegal && tall;
    var isRoyalty = status === 'royalty';
    var specialCase = isRoyalty && hasInvitation;
    var canEnterOurBar = suitable || specialCase;  
}
is();
console.log('----------------------');
console.log(isLegal);
console.log(tall);
console.log(suitable);
console.log(isRoyalty);
console.log(specialCase);
console.log(canEnterOurBar);
console.log('----------------------');*/

function yu(){
    for(var i = 0; i < 3; i++){
        if (i === 1){
            continue;
        }
        console.log(i);
    }
    
}
yu();
console.log('----------------------');
//ejercicio 8.2
function gh(){
    var i=0;
    while (i<3){
        if(i===1){
            i=2;
            continue;
        }
        console.log(i);
        i++;
    }
}
gh();
console.log('----------------------');


//ejercicio 9.1
/*function nex(){
    for(var i=0; i< 5; i++){
        nextLoop2Iteration;
        for(var j=0; j<5; j++){
            if(i== j) break nextLoop2Iteration;
        }
    }
}
nex();*/
console.log('----------------------');


//ejercicio 9.2
/*function foo(){
    var a = 'hello';
    function bar(){
        var b='world';
        console.log(a);
        console.log(b);
    }
    bar();
    console.log(a);
    console.log(b);
}
foo();
console.log(a);
console.log(b);*/


//ejercicio 10.1
/*function foo(){
    const a= true;
    function bar(){
        const a= false;
        console.log(a);
    }
    bar();
    const a= false;
    a=false;
    console.log(a);
}
foo();*/

//ejercicio 10.2
function gjk(){
    var namedSum = function sum(a,b){
        return a+b;
    }
    var anonSum = function (a,b){
        return a+b;
    }
    namedSum(1,3);
    anonSum(1,3);
}
gjk();


//ejercicio 11.1