const pets =[
    {name:'lucho', species: 'dog'},
    {name:'felix', species: 'cat'},
    {name:'nemo', species: 'fish'}];

var petNames = [];
for (let i = 0; i < pets.length; i++) {
    petNames.push( pets[i].name);
    
}

console.log(`Pets names >> : ${petNames}`);
console.log(`Ejemplo template string A : ${5+5}`);
console.log(`jemplo template string B: ${petNames.length > 3 ? "ok" : "cancel"}`);

//arrow fuction
const  petNames = pets.map(animal => animal.name);
//un objeto map puede iterar sobre sus elementos en orden de inserccion
console.log(`Pets names with map: ${petNames}`);