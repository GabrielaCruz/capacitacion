//pagina 28
/*foma tradicional*/
var array = new Array('a','b','c');

/*mediante literakes (preferida)*/
var array =['a','b','c'];
var empty =[];
var mixto =['a',5,true];

/*console.log(array);
console.log(empty);
console.log(mixto);*/


//pagina 29

var a= new Array(3);
var b= [2];//incorrecto

//pagina 31
var array = ['a','b','c'];
array.push('d');
array.pop();

array.unshift('Z');
array.shift();

//pagina 31.2
var array1=[1,2,3];
array1.push(4,5,6);
array1.push([7,8,9]);
console.log(array1);

var array2 =[1,2,3];
array2= array2.concat(4,5,6);
array2 = array2.concat([7,8,9]);
console.log(array2);

//pagina 34
var array3 = [1,8,2,32,9,7,4];

var f=function(a,b){
    if(a>b){
        return 1;
    }else{
        return -1;
    }
    return 0;
}
console.log(array3.sort(f));