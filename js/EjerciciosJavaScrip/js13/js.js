// === arrow fuction
const numero = (num) =>{
    return parseInt(num)
}

//esta seria la refactorizacion de los ejemplos anteriores
const numero = num => parseInt(num);
const suma = (a,b) => numero(a) + numero(b);

function myfunc(args1){
    return "algo";
}
myfunc();

let myfuncVar = myfunc;
myfuncVar("param");

let myfuncVar2 = function doSomething(args1){
    return "doSomething";
};
myfuncVar2("param")