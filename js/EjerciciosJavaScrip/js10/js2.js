const req = new XMLHttpRequest();
req.addEventListener('load',()=>{
    //procesa los datos de devolucion si el estatus es 200
    if (req.status===200) {
        // que se tiene que hacer con los datos

        //formato texto
        enconsola(JSON.parse(req.responseText));
        enPantalla(req.responseText);
    } else {
        //que hacemos cuando falla
        enconsola([req.status, req.statusText]);
    }
});

req.addEventListener('error',()=>{
    enconsola('error de red');
});

// se abre una nueva solicitud asicnrona true

req.open('GET', 'https://jsonplaceholder.typicode.com/posts',true);
req.send(null);